class FightService {
    constructor(controls, firstFighter, secondFighter) {
        this.pressed = new Set();

        this.firstFighter = {
            ...firstFighter,
            isBlocking: false,
        };

        this.secondFighter = {
            ...secondFighter,
            isBlocking: false,
        };
    }

    keyDownEvent(event) {
        
    }
}

export default FightService;