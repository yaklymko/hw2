import {showModal} from './modal';

export function showWinnerModal(fighter) {
  showModal({
    title: "We have a winner!",
    bodyElement: `Fighter ${fighter.name} won!`
  });
}
