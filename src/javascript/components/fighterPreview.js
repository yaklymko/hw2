import {
  createElement
} from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  if (!fighter) {
    return "";
  }
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });


  const imageElement = createFighterImage(fighter);
  const fighterDescription = createFighterDescription(fighter);

  fighterElement.append(fighterDescription);
  fighterElement.append(imageElement);

  return fighterElement;
}

function createFighterDescription(fighter) {
  const {
    source,
    _id,
    ...stats
  } = fighter;

  const statsList = createElement({
    tagName: "span",
    className: 'fighter-preview___stats',
  });

  for (let statName of Object.keys(stats)) {
    const statsListItem = createElement({
      tagName: "p"
    });

    statsListItem.textContent = `${statName.charAt(0).toUpperCase() + statName.slice(1)} : [${stats[statName]}]`;
    statsList.appendChild(statsListItem);
  }
  return statsList;

}

export function createFighterImage(fighter) {
  const {
    source,
    name
  } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}