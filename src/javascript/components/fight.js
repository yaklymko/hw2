import {
  controls
} from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {

  return new Promise((resolve) => {
    document.addEventListener("keydown", keyDownEvent);
    document.addEventListener("keyup", keyUpEvent);

    const pressed = new Set();
    // I'd use copy but it can't pass tests 
    firstFighter.isBlocking = false;
    firstFighter.superAttackCodes = controls.PlayerOneCriticalHitCombination;
    firstFighter.hitBarPart = 100 / firstFighter.health;
    firstFighter.hasSuperAttack = true;
    firstFighter.resolve = resolve;



    secondFighter.isBlocking = false;
    secondFighter.superAttackCodes = controls.PlayerTwoCriticalHitCombination;
    secondFighter.hitBarPart = 100 / secondFighter.health;
    secondFighter.hasSuperAttack = true;
    secondFighter.resolve = resolve;

    // renderHitbars(firstFighter, secondFighter);


    function keyDownEvent(event) {
      if (event.repeat) {
        // checks if a key is being held down
        return;
      }

      pressed.add(event.code);

      if (checkSuperAttacks(firstFighter, secondFighter)) {
        renderHitbars(firstFighter, secondFighter);
        return;
      }

      switch (event.code) {

        case controls.PlayerOneAttack: {
          getDamage(firstFighter, secondFighter);
        }
        break;

      case controls.PlayerTwoAttack: {
        getDamage(secondFighter, firstFighter);
      }
      break;

      case controls.PlayerOneBlock: {
        firstFighter.isBlocking = true;
      }
      break;

      case controls.PlayerTwoBlock: {
        secondFighter.isBlocking = true;
      }
      break;

      }
      renderHitbars(firstFighter, secondFighter);
    }

    function keyUpEvent(event) {
      pressed.delete(event.code);
      switch (event.code) {
        case controls.PlayerOneBlock: {
          firstFighter.isBlocking = false;
        }
        break;

      case controls.PlayerTwoBlock: {
        secondFighter.isBlocking = false;
      }
      break;
      }
    }

    function checkSuperAttacks(firstFighter, secondFighter) {
      if (isSuperAttack(firstFighter) && firstFighter.hasSuperAttack) {
        firstFighter.hasSuperAttack = false;
        setTimeout(() => {
          firstFighter.hasSuperAttack = true;
        }, 10000);
        getCritDamage(firstFighter, secondFighter);
        return;
      }

      if (isSuperAttack(secondFighter) && secondFighter.hasSuperAttack) {
        secondFighter.hasSuperAttack = false;
        setTimeout(() => {
          secondFighter.hasSuperAttack = true;
        }, 10000);
        getCritDamage(secondFighter, firstFighter);
        return;
      }
    }

    function isSuperAttack(fighter) {
      for (let code of fighter.superAttackCodes) {
        if (!pressed.has(code)) {
          return;
        }
      }
      return true;
    }
  })

}

export function getCritDamage(attacker, defender) {
  // Render hitbar
  let dmg = getCritHitPower(attacker);
  defender.health -= dmg;

  if (defender.health <= 0) {
    defender.health = 0;
    attacker.resolve(attacker);
  }
  return dmg;
}

export function renderHitbars(firstFighter, secondFighter) {
  document.getElementById("left-fighter-indicator").style.width = firstFighter.health * firstFighter.hitBarPart + "%";
  document.getElementById("right-fighter-indicator").style.width = secondFighter.health * secondFighter.hitBarPart + "%";
}

export function getCritHitPower(fighter) {
  return fighter.attack * 2;
}

export function getDamage(attacker, defender) {
  if (defender.isBlocking || attacker.isBlocking) {
    return;
  }
  let dmg = getHitPower(attacker) - getBlockPower(defender);
  if (dmg < 0) {
    dmg = 0;
  }
  defender.health -= dmg;

  if (defender.health <= 0) {
    defender.health = 0;
    attacker.resolve(attacker);
  }

  return dmg;
}

export function getHitPower(fighter) {
  return fighter.attack * (Math.random() + 1);
}

export function getBlockPower(fighter) {
  return fighter.defense * (Math.random() + 1);
}